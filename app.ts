import express from "express";
import ConfigureCORS from "./middlewares/configure/cors";
import ConfigureParser from "./middlewares/configure/parser";
import RoutesRegister from "./routes";
import wisudawanRouter from "./routes/wisudawan";
import himpunanRouter from "./routes/himpunan";
import ukmRouter from "./routes/ukm";
import tafairRouter from "./routes/tafair";
import storyRouter from "./routes/story";

const port = process.env.PORT ?? 8888;
const app = express();

ConfigureCORS(app);
ConfigureParser(app);

// app.use("/wisudawan", wisudawanRouter);
app.use("/himpunan", himpunanRouter);
app.use("/story", storyRouter);
app.use("/ukm", ukmRouter);
app.use("/tafair", tafairRouter);

RoutesRegister(app);

app.listen(port, () => {
  console.log(`🌍 Server is running on port ${port}.`);
});
