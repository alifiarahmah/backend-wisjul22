import express from "express";
import { prisma } from "../services/db/prisma";
import verifyToken from "../services/google/authentication";
import Request from "../models/Request";
export interface AuthenticateOptions {
  isOnlyAdmin?: boolean;
  isAllowNewUser?: boolean;
}

export default function AuthMiddleware(
  options: AuthenticateOptions = {}
): express.Handler {
  return async (req: Request, res, next) => {
    try {
      const auth = req.headers.authorization?.split(" ")[1];

      if (!auth) {
        return res.status(400).json({
          status: "failed",
          message: "Token is not provided",
        });
      }

      const userData = await verifyToken(auth);
      if (!userData) {
        return res.status(401).json({
          status: "failed",
          message: "Invalid Token",
        });
      }

      const data = await prisma.pengguna.findUnique({
        where: {
          email: userData.email,
        },
      });

      if (!data && !options.isAllowNewUser) {
        return res.status(401).json({
          status: "failed",
          message: "User is not registered",
        });
      }

      if (options.isOnlyAdmin && !data?.is_admin) {
        return res.status(403).json({
          status: "failed",
          message: "User is not allowed to access this resources",
        });
      }

      if (data?.is_blocked) {
        return res.status(403).json({
          status: "failed",
          message: "User is blocked to access this resources",
        });
      }

      req.user = data ?? {
        email: userData.email,
        nama: userData.name,
        is_admin: false,
        is_blocked: false,
        institusi: null,
        fakultas: null,
        id_pengguna: "",
      };

      next();
    } catch (err) {
      console.error(`[ERROR] ${err}`);
      res.status(401).json({
        status: "failed",
        message: "Invalid Token",
      });
    }
  };
}
