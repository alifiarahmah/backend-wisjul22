import { type Express } from "express";
import express from "express";
import bodyParser from "body-parser";

export default function ConfigureParser(app: Express) {
  app.use(express.json());
  app.use(bodyParser.urlencoded({ extended: false }));
}
