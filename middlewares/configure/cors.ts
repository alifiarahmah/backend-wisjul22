import { type Express } from "express";

const cors = require("cors");
const corsOptions = {
  origin: [
    "http://localhost:3000",
    "https://devparadewisudaitb.netlify.app",
    "https://paradewisudaitb.com",
  ],
  credentials: true,
  methods: ["GET", "PUT", "POST", "PATCH", "OPTIONS"],
};

export default function ConfigureCORS(app: Express) {
  app.use(cors(corsOptions));

  // Prefligt CORS
  app.options("*", cors(corsOptions));
}
