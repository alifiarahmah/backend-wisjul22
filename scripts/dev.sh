#!/bin/bash

yarn install
npx prisma migrate dev
npx nodemon --legacy-watch app.ts
