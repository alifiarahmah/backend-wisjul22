#!/bin/bash

npm i
npx babel . --ignore node_modules --out-dir ./build --extensions '.ts,.js'
