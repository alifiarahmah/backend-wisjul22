/*
  Warnings:

  - The primary key for the `Komentar` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The primary key for the `Pengguna` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - A unique constraint covering the columns `[email]` on the table `Pengguna` will be added. If there are existing duplicate values, this will fail.

*/
-- DropForeignKey
ALTER TABLE "Komentar" DROP CONSTRAINT "Komentar_id_pengguna_fkey";

-- AlterTable
ALTER TABLE "Komentar" DROP CONSTRAINT "Komentar_pkey",
ALTER COLUMN "id_pengguna" DROP DEFAULT,
ALTER COLUMN "id_pengguna" SET DATA TYPE TEXT,
ADD CONSTRAINT "Komentar_pkey" PRIMARY KEY ("NIM_wisudawan", "id_pengguna");
DROP SEQUENCE "Komentar_id_pengguna_seq";

-- AlterTable
ALTER TABLE "Pengguna" DROP CONSTRAINT "Pengguna_pkey",
ALTER COLUMN "id_pengguna" DROP DEFAULT,
ALTER COLUMN "id_pengguna" SET DATA TYPE TEXT,
ALTER COLUMN "is_admin" SET DEFAULT false,
ALTER COLUMN "is_blocked" SET DEFAULT false,
ADD CONSTRAINT "Pengguna_pkey" PRIMARY KEY ("id_pengguna");
DROP SEQUENCE "Pengguna_id_pengguna_seq";

-- CreateIndex
CREATE UNIQUE INDEX "Pengguna_email_key" ON "Pengguna"("email");

-- AddForeignKey
ALTER TABLE "Komentar" ADD CONSTRAINT "Komentar_id_pengguna_fkey" FOREIGN KEY ("id_pengguna") REFERENCES "Pengguna"("id_pengguna") ON DELETE RESTRICT ON UPDATE CASCADE;
