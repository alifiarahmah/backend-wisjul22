-- CreateTable
CREATE TABLE "Wisudawan" (
    "NIM" INTEGER NOT NULL,
    "nama" VARCHAR(150) NOT NULL,
    "foto_url" TEXT NOT NULL,
    "id_himpunan" INTEGER NOT NULL,
    "judul_TA" TEXT NOT NULL,

    CONSTRAINT "Wisudawan_pkey" PRIMARY KEY ("NIM")
);

-- CreateTable
CREATE TABLE "Komentar" (
    "id_pengguna" SERIAL NOT NULL,
    "NIM_wisudawan" INTEGER NOT NULL,
    "nama_pengirim" VARCHAR(200) NOT NULL,
    "komentar" TEXT NOT NULL,
    "is_anonim" BOOLEAN NOT NULL,

    CONSTRAINT "Komentar_pkey" PRIMARY KEY ("NIM_wisudawan","id_pengguna")
);

-- CreateTable
CREATE TABLE "Pengguna" (
    "id_pengguna" SERIAL NOT NULL,
    "nama" TEXT NOT NULL,
    "email" TEXT NOT NULL,
    "is_admin" BOOLEAN NOT NULL,
    "is_blocked" BOOLEAN NOT NULL,

    CONSTRAINT "Pengguna_pkey" PRIMARY KEY ("id_pengguna")
);

-- CreateTable
CREATE TABLE "Story" (
    "NIM_wisudawan" INTEGER NOT NULL,
    "html_story" TEXT NOT NULL,

    CONSTRAINT "Story_pkey" PRIMARY KEY ("NIM_wisudawan")
);

-- CreateTable
CREATE TABLE "Prestasi" (
    "id_prestasi" SERIAL NOT NULL,
    "nama_prestasi" VARCHAR(200) NOT NULL,
    "tanggal" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Prestasi_pkey" PRIMARY KEY ("id_prestasi")
);

-- CreateTable
CREATE TABLE "Kepanitiaan" (
    "id_panitia" SERIAL NOT NULL,
    "nama_kepanitiaan" VARCHAR(200) NOT NULL,
    "jabatan" VARCHAR(200) NOT NULL,
    "tanggal" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Kepanitiaan_pkey" PRIMARY KEY ("id_panitia")
);

-- CreateTable
CREATE TABLE "Himpunan" (
    "id_himpunan" SERIAL NOT NULL,
    "nama_himpunan" VARCHAR(200) NOT NULL,
    "singkatan" VARCHAR(5) NOT NULL,
    "deskripsi" TEXT NOT NULL,
    "fakultas" VARCHAR(5) NOT NULL,

    CONSTRAINT "Himpunan_pkey" PRIMARY KEY ("id_himpunan")
);

-- CreateTable
CREATE TABLE "Organisasi" (
    "id_organisasi" SERIAL NOT NULL,
    "nama" VARCHAR(150) NOT NULL,
    "deskripsi" TEXT NOT NULL,
    "singkatan" VARCHAR(5) NOT NULL,
    "logo_url" TEXT NOT NULL,
    "is_UKM" BOOLEAN NOT NULL,
    "kategori" VARCHAR(5) NOT NULL,

    CONSTRAINT "Organisasi_pkey" PRIMARY KEY ("id_organisasi")
);

-- CreateTable
CREATE TABLE "PrestasiWisudawan" (
    "id_prestasi" INTEGER NOT NULL,
    "NIM_wisudawan" INTEGER NOT NULL,

    CONSTRAINT "PrestasiWisudawan_pkey" PRIMARY KEY ("NIM_wisudawan","id_prestasi")
);

-- CreateTable
CREATE TABLE "KepanitiaanWisudawan" (
    "id_panitia" INTEGER NOT NULL,
    "NIM_wisudawan" INTEGER NOT NULL,

    CONSTRAINT "KepanitiaanWisudawan_pkey" PRIMARY KEY ("NIM_wisudawan","id_panitia")
);

-- CreateTable
CREATE TABLE "OrganisasiWisudawan" (
    "id_organisasi" INTEGER NOT NULL,
    "NIM_wisudawan" INTEGER NOT NULL,

    CONSTRAINT "OrganisasiWisudawan_pkey" PRIMARY KEY ("NIM_wisudawan","id_organisasi")
);

-- AddForeignKey
ALTER TABLE "Wisudawan" ADD CONSTRAINT "Wisudawan_id_himpunan_fkey" FOREIGN KEY ("id_himpunan") REFERENCES "Himpunan"("id_himpunan") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Komentar" ADD CONSTRAINT "Komentar_NIM_wisudawan_fkey" FOREIGN KEY ("NIM_wisudawan") REFERENCES "Wisudawan"("NIM") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Komentar" ADD CONSTRAINT "Komentar_id_pengguna_fkey" FOREIGN KEY ("id_pengguna") REFERENCES "Pengguna"("id_pengguna") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Story" ADD CONSTRAINT "Story_NIM_wisudawan_fkey" FOREIGN KEY ("NIM_wisudawan") REFERENCES "Wisudawan"("NIM") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PrestasiWisudawan" ADD CONSTRAINT "PrestasiWisudawan_NIM_wisudawan_fkey" FOREIGN KEY ("NIM_wisudawan") REFERENCES "Wisudawan"("NIM") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PrestasiWisudawan" ADD CONSTRAINT "PrestasiWisudawan_id_prestasi_fkey" FOREIGN KEY ("id_prestasi") REFERENCES "Prestasi"("id_prestasi") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "KepanitiaanWisudawan" ADD CONSTRAINT "KepanitiaanWisudawan_NIM_wisudawan_fkey" FOREIGN KEY ("NIM_wisudawan") REFERENCES "Wisudawan"("NIM") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "KepanitiaanWisudawan" ADD CONSTRAINT "KepanitiaanWisudawan_id_panitia_fkey" FOREIGN KEY ("id_panitia") REFERENCES "Kepanitiaan"("id_panitia") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganisasiWisudawan" ADD CONSTRAINT "OrganisasiWisudawan_NIM_wisudawan_fkey" FOREIGN KEY ("NIM_wisudawan") REFERENCES "Wisudawan"("NIM") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrganisasiWisudawan" ADD CONSTRAINT "OrganisasiWisudawan_id_organisasi_fkey" FOREIGN KEY ("id_organisasi") REFERENCES "Organisasi"("id_organisasi") ON DELETE RESTRICT ON UPDATE CASCADE;
