import type express from "express";
import { type Express } from "express";
import AuthRegister from "./auth";
import ErrorRegister from "./error";
import WisudawanRegister from "./wisudawan";

const indexRoute = (req: express.Request, res: express.Response) => {
  res.json({
    status: "success",
    message: "server is running",
  });
};

export default function RoutesRegister(app: Express) {
  app.get("/", indexRoute);

  AuthRegister(app);
  WisudawanRegister(app);

  ErrorRegister(app);
}
