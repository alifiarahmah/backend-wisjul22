import type express from "express";
import { type Express } from "express";

const ErrorPage404 = (req: express.Request, res: express.Response) => {
  res.status(404).json({
    status: "failed",
    message: "Route is not exist",
  });
};

export default function ErrorRegister(app: Express) {
  // Should register error page in the last definition
  app.use("*", ErrorPage404);
}
