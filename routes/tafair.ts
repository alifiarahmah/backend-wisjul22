import { nextTick } from "process";
import { type Express } from "express";
import { PostCommentTA } from "../controllers/comment";
import AuthMiddleware from "../middlewares/auth";

const router = require("express").Router();
const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

router.get("/", async (req: any, res: any) => {
  try {
    //const tugasAkhir = await prisma.tugasAkhir.findMany()
    const tugasAkhir =
      await prisma.$queryRaw`SELECT "TugasAkhir"."id_ta","TugasAkhir"."judulTA","TugasAkhir"."link_portofolio", 
        "TugasAkhir"."dosen_pembimbing","TugasAkhir"."abstrak","TugasAkhir"."cerita_TA",
        "TugasAkhir"."cerita_kuliah","TugasAkhir"."is_porto_file","Wisudawan"."nama" AS "nama_wisudawan","Wisudawan"."NIM",
        "Wisudawan"."moto_hidup","Jurusan".singkatan AS "singkatan_jurusan","Jurusan".nama AS "nama_jurusan"
        ,"Jurusan".kode_fakultas
         FROM "TugasAkhir" LEFT JOIN "Wisudawan" ON "Wisudawan"."id_ta"="TugasAkhir"."id_ta"
         JOIN "Jurusan" ON "Wisudawan".id_jurusan="Jurusan".id_jurusan`;

    const data = (tugasAkhir as any[]).reduce((cum, cur) => {
      if (cum.length > 0 && cum[cum.length - 1].id_ta === cur.id_ta) {
        cum[cum.length - 1].wisudawan.push({
          nim: cur.NIM,
          nama: cur.nama_wisudawan,
        });

        return cum;
      } else {
        const { NIM, nama_wisudawan, ...others } = cur;

        return [
          ...cum,
          {
            ...others,
            wisudawan: [
              {
                nim: NIM,
                nama: nama_wisudawan,
              },
            ],
          },
        ];
      }
    }, []);
    res.json({
      status: "success",
      data,
      //nama,nim,jurusan,fakultas,moto hidup
    });
  } catch (error) {
    res.status(400).json({
      status: "error",
      message: error,
    });
  }
});

router.post("/", async (req: any, res: any) => {
  try {
    const tugasAkhir = await prisma.tugasAkhir.create({
      data: req.body,
    });
    res.json({
      status: "success",
      data: tugasAkhir,
    });
  } catch (error) {
    res.status(400).json({
      status: "error",
      message: error,
    });
  }
});

router.get("/:id", async (req: any, res: any, next: any) => {
  try {
    const { id } = req.params;
    //const tugasAkhir = await prisma.tugasAkhir.findMany({
    //    where: {
    //        id_ta : id
    //    }
    //})
    if (!isNaN(parseInt(id))) {
      //kalo di cast ke integer, hasilya bukan NaN berarti valid
      const tugasAkhir =
        await prisma.$queryRaw`SELECT "TugasAkhir"."id_ta","TugasAkhir"."judulTA","TugasAkhir"."link_portofolio", 
          "TugasAkhir"."dosen_pembimbing","TugasAkhir"."abstrak","TugasAkhir"."cerita_TA",
          "TugasAkhir"."cerita_kuliah","TugasAkhir"."is_porto_file","Wisudawan"."nama" AS "nama_wisudawan","Wisudawan"."NIM",
          "Wisudawan"."moto_hidup","Jurusan".singkatan AS "singkatan_jurusan","Jurusan".nama AS "nama_jurusan"
          ,"Jurusan".kode_fakultas
           FROM "TugasAkhir" LEFT JOIN "Wisudawan" ON "Wisudawan"."id_ta"="TugasAkhir"."id_ta" AND "TugasAkhir".id_ta=${Number(
             id
           )}
           JOIN "Jurusan" ON "Wisudawan".id_jurusan="Jurusan".id_jurusan`;
      const komentarTA = await prisma.komentarTA.findMany({
        where: {
          id_ta: Number(id),
        },
      });

      const data = (tugasAkhir as any[]).reduce((cum, cur) => {
        if (!cum) {
          return {
            ...cur,
            wisudawan: [
              {
                nim: cur.NIM,
                nama: cur.nama_wisudawan,
              },
            ],
          };
        } else {
          cum.wisudawan.push({
            nim: cur.NIM,
            nama: cur.nama_wisudawan,
          });

          return cum;
        }
      }, null);

      res.json({
        status: "success",
        data,
        komentar: komentarTA,
      });
    }
  } catch (error) {
    next(error);
  }
});
router.post("/:id/comment", AuthMiddleware(), PostCommentTA);
export default router;

//export default function TAFairRegister(app: Express) {
//  app.post("/tafair/:id/comment", AuthMiddleware(), PostCommentTA);
//}
