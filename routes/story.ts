const router = require("express").Router();
const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

router.get('/', async(req: any, res: any) => {
    try {
        const stories = await prisma.story.findMany({
            include:{
                story_to_wisudawan:{
                    select:{
                        nama:true
                    }
                }
            }
        })
        res.json({
            status: "success",
            data: stories
        });
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: error
        });
    }
})

router.get('/:nim', async(req: any, res: any) => {
    try {
        const { nim } = req.params;
        const story = await prisma.story.findUnique({
            where: {
                NIM_wisudawan : Number(nim)
            },
            include:{
                story_to_wisudawan:{
                    select:{
                        nama:true
                    }
                }
            }
        });
        res.json({
            status: "success",
            data: story
        });
    } catch (error) {
        res.status(400).json({
            status: "failed",
            message: error
        });
    }
})

router.post('/', async(req: any, res: any) => {
    try {
        const { nim, html_story } = req.body;
        const new_story = await prisma.story.create({
            data: {nim, html_story}
        });
        res.json({
            status: "success",
            message: "new story created"
        });
    } catch (error) {
        res.json({
            status: "failed",
            message: error
        });
    }
})

export default router;