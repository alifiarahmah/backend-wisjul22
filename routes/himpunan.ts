const router = require('express').Router()
const {PrismaClient} = require("@prisma/client")

const prisma = new PrismaClient()

router.get('/',async (req: any, res: any, next: any) => {
    try {
        const himpunan = await prisma.himpunan.findMany()
        res.json({
            status: "success",
            data: himpunan
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            data: [],
          });
    }
})

router.get('/:singkatan', async (req: any, res: any, next: any) => {
    try {
        const { singkatan } = req.params
        const himpunan = await prisma.himpunan.findMany({
            where: {
                singkatan : singkatan
            }
        })
        res.json({
            status: "success",
            data: himpunan
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            data: [],
          });
    }
})

router.get('/:singkatan/wisudawan', async (req: any, res: any, next: any) => {
    try {
        const { singkatan } = req.params
        const himpunan = await prisma.himpunan.findMany({
            select: {
                id_himpunan: true
            },
            where:{
                singkatan:singkatan
            }
        })
        const id_himpunan = himpunan[0]["id_himpunan"]
        const wisudawan = await prisma.wisudawan.findMany({
            where: {
                id_himpunan: id_himpunan
            },
            include:{
                wisudawan_to_jurusan: {
                    select:{
                        singkatan:true
                    }
                }
            }
        })
        res.json({
            status: "success",
            data: wisudawan
        })
    } catch (error) {
        console.log(error)
        res.status(400).json({
            status: "failed",
            data: [],
          });
    }
})

export default router