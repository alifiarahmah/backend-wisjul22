const router = require('express').Router()
const {PrismaClient} = require("@prisma/client")

const prisma = new PrismaClient()

router.get('/', async (req: any, res: any, next: any) => {
    try {
        const organisasi = await prisma.organisasi.findMany()
        res.json({
            status: "success",
            data: organisasi
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            data: [],
          });
    }
})

router.get('/:singkatan', async (req: any, res: any, next: any) => {
    try {
        const { singkatan } = req.params
        const organisasi = await prisma.organisasi.findMany({
            where: {
                singkatan : singkatan
            }
        })
        res.json({
            status: "success",
            data: organisasi
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            data: [],
          });
    }
})

router.get('/:singkatan/wisudawan', async (req: any, res: any, next: any) => {
    try {
        const { singkatan } = req.params
        const wisudawan = await prisma.wisudawan.findMany({
            where: {
                organisasi: {
                    some: {
                        organisasi: {
                            singkatan: singkatan
                        }
                    }
                }
            },
            include:{
                wisudawan_to_jurusan: {
                    select:{
                        singkatan:true
                    }
                }
            }
        })
        res.json({
            status: "success",
            data: wisudawan
        })
    } catch (error) {
        res.status(400).json({
            status: "failed",
            data: [],
          });
    }
})

export default router