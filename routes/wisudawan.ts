import { type Express } from "express";
import { PostComment } from "../controllers/comment";
import AuthMiddleware from "../middlewares/auth";

const { PrismaClient } = require("@prisma/client");

const router = require("express").Router();
const prisma = new PrismaClient();

const GetAllWisudawan = async (req: any, res: any) => {
  try {
    const wisudawan = await prisma.wisudawan.findMany();
    res.json({
      status: "success",
      data: wisudawan,
    });
  } catch (error) {
    res.status(400).json({
      status: "error",
      message: error,
    });
  }
};

const GetWisudawanByNim = async (req: any, res: any) => {
  try {
    const { nim } = req.params;
    const wisudawan = await prisma.wisudawan.findUnique({
      where: {
        NIM: Number(nim),
      },
    });
    const komentar = await prisma.komentar.findMany({
      where: {
        NIM_wisudawan: Number(nim),
      },
    });
    const prestasi = await prisma.prestasi.findMany({
      where: {
        wisudawan: {
          some: {
            wisudawan: {
              NIM: Number(nim),
            }
          }
        }
      },
    });
    const organisasi = await prisma.organisasi.findMany({
      where: {
        wisudawan: {
          some: {
            wisudawan: {
              NIM: Number(nim),
            }
          }
        }
      },
    });
    const jurusan = await prisma.jurusan.findUnique({
      where: {
        id_jurusan: wisudawan["id_jurusan"],
      },
    });
    const kepanitiaan = await prisma.kepanitiaan.findMany({
      where: {
        wisudawan: {
          some: {
            wisudawan: {
              NIM: Number(nim),
            }
          }
        }
      },
    });
    res.json({
      status: "success",
      data: {
        "wisudawan": wisudawan,
        "komentar": komentar,
        "prestasi": prestasi,
        "organisasi": organisasi,
        "jurusan": jurusan,
        "kepanitiaan": kepanitiaan
      }
    });
  } catch (error) {
    res.status(400).json({
      status: "failed",
      message: error,
    });
  }
};

// router.get("/", GetAllWisudawan);
// router.get("/:nim",GetWisudawanByNim);
// router.post("/:nim/comment",AuthMiddleware(),PostComment);
// export default router;
export default function WisudawanRegister(app: Express) {
 app.get("/wisudawan", GetAllWisudawan);
 app.get("/wisudawan/:nim", GetWisudawanByNim);
 app.post("/wisudawan/:nim/comment", AuthMiddleware(), PostComment);
}
