import { type Express } from "express";
import { PatchAccount, PatchAccountByAdmin } from "../controllers/Account";
import PostLogin from "../controllers/Login";
import AuthMiddleware from "../middlewares/auth";

export default function AuthRegister(app: Express) {
  app.post("/login", AuthMiddleware({ isAllowNewUser: true }), PostLogin);
  app.patch("/account", AuthMiddleware(), PatchAccount);
  app.patch("/account/:userId", AuthMiddleware(), PatchAccountByAdmin);
}
