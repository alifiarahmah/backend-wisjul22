export interface IGoogleUser {
  name: string;
  email: string;
  isVerified: boolean;
}
