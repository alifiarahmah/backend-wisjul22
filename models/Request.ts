import { Pengguna } from "@prisma/client";
import express from "express";

export default interface Request extends express.Request {
  user?: Pengguna;
}
