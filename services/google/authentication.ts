import { OAuth2Client } from "google-auth-library";
import { IGoogleUser } from "../../models/IUser";

const client = new OAuth2Client({
  clientId: process.env.GSI_CLIENT_ID,
});

export default async function verifyToken(token: string) {
  const ticket = await client.verifyIdToken({
    idToken: token,
  });

  const payload = ticket.getPayload();

  if (payload?.email && payload?.name && payload?.email_verified) {
    return {
      email: payload.email,
      name: payload.name,
      isVerified: payload.email_verified,
    } as IGoogleUser;
  }

  return null;
}
