FROM node:lts

COPY . /app
WORKDIR /app

RUN ["/bin/bash", "./scripts/build.sh"]
CMD ["/bin/bash", "./scripts/start.sh"]
