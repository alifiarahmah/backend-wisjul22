import Request from "../models/Request";
import express from "express";
import { IComment } from "../models/IComment";
import { prisma } from "../services/db/prisma";
import validator from "validator";

export async function PostComment(req: Request, res: express.Response) {
  try {
    const body = req.body as IComment;
    const { nim } = req.params;

    if (!req.user) {
      console.error("[ERROR] User object is required in Post Comment");
      return res.status(500).json({
        status: "failed",
        message: "Internal server error",
      });
    }

    if (
      !nim ||
      !body ||
      body.is_anonim === undefined ||
      body.komentar === undefined
    ) {
      return res.status(400).json({
        status: "failed",
        message: "Insuffient data",
      });
    }

    if (!validator.isInt(nim)) {
      return res.status(400).json({
        status: "failed",
        message: "nim field must be integer",
      });
    }

    const length = await prisma.komentar.count({
      where: {
        id_pengguna: req.user.id_pengguna,
      },
    });

    if (length > 0) {
      return res.status(400).json({
        status: "failed",
        mesaage: "duplicate data",
      });
    }

    const data = await prisma.komentar.create({
      data: {
        is_anonim: !!body.is_anonim,
        komentar: body.komentar,
        nama_pengirim: !body.is_anonim ? req.user.nama : "Anonim",
        id_pengguna: req.user.id_pengguna,
        NIM_wisudawan: parseInt(nim),
      },
    });

    return res.json({
      status: "success",
      message: "Message was sended successfully",
      data,
    });
  } catch (err) {
    console.error(`[ERROR] ${err}`);
    res.status(500).json({
      message: "Internal server error",
      status: "failed",
    });
  }
}

export async function PostCommentTA(req: Request, res: express.Response) {
  const body = req.body as IComment;
  const { id } = req.params;

  if (!req.user) {
    console.error("[ERROR] User object is required in Post Comment");
    return res.status(500).json({
      status: "failed",
      message: "Internal server error",
    });
  }

  if (!id || body.is_anonim === undefined || !body.komentar) {
    return res.status(400).json({
      status: "failed",
      message: "Insuffient data",
    });
  }

  if (!validator.isInt(id)) {
    return res.status(400).json({
      status: "failed",
      message: "Id is not valid. Id must be integer",
    });
  }

  const length = await prisma.komentarTA.count({
    where: {
      id_pengguna: req.user.id_pengguna,
    },
  });

  if (length > 0) {
    return res.status(400).json({
      status: "failed",
      mesaage: "duplicate data",
    });
  }

  try {
    const data = await prisma.komentarTA.create({
      data: {
        is_anonim: !!body.is_anonim,
        komentar: body.komentar,
        nama_pengirim: !body.is_anonim ? req.user.nama : "Anonim",
        id_pengguna: req.user.id_pengguna,
        id_ta: parseInt(id),
      },
    });

    return res.json({
      status: "success",
      message: "Message was sended successfully",
      data,
    });
  } catch (err) {
    console.error(`[ERROR] ${err}`);
    res.status(500).json({
      message: "Internal server error",
      status: "failed",
    });
  }
}
