import { prisma } from "../services/db/prisma";
import express from "express";
import Request from "../models/Request";

export default async function PostLogin(req: Request, res: express.Response) {
  try {
    let user = req.user;

    if (!user) {
      console.error("[ERROR] Login object is required in PostLogin");
      return res.status(500).json({
        status: "error",
        message: "Internal server error",
      });
    }

    if (!user?.id_pengguna) {
      user = await prisma.pengguna.create({
        data: {
          email: user.email,
          nama: user.nama,
          is_admin: false,
          is_blocked: false,
        },
      });
    }

    res.json({
      status: "success",
      message: "User is granted",
      data: {
        name: user.nama,
        email: user.email,
        faculty: user.fakultas,
        institution: user.institusi,
        is_new: !user.institusi || !user.fakultas,
      },
    });
  } catch (err) {
    console.error(`[ERROR] ${err}`);
    res.status(500).json({
      status: "error",
      message: "Internal Server Error",
    });
  }
}
