import { prisma } from "../services/db/prisma";
import type express from "express";
import Request from "../models/Request";
import { Pengguna } from "@prisma/client";

export async function PatchAccount(req: Request, res: express.Response) {
  const user = req.user;

  if (!user) {
    console.error("[ERROR] Login object is required in PatchProfile");
    return res.status(500).json({
      status: "error",
      message: "Internal server error",
    });
  }

  const data = req.body as Pengguna;

  if (
    typeof data.nama !== "string" ||
    typeof data.fakultas !== "string" ||
    typeof data.institusi !== "string"
  ) {
    return res.status(400).json({
      status: "error",
      message: "Invalid field value",
    });
  }

  const newUser: Pengguna = {
    ...user,
    nama: data.nama ?? user.nama,
    fakultas: data.fakultas ?? user.fakultas,
    institusi: data.institusi ?? user.institusi,
  };

  try {
    await prisma.pengguna.update({
      where: {
        id_pengguna: user.id_pengguna,
      },
      data: newUser,
    });

    res.json({
      status: "success",
      message: "User account edited",
    });
  } catch (err) {
    console.error(`[ERROR] ${err}`);
    res.status(500).json({
      message: "Internal server error",
      status: "error",
    });
  }
}

export async function PatchAccountByAdmin(req: Request, res: express.Response) {
  const user = req.user;

  if (!user) {
    console.error("[ERROR] Login object is required in PutAccountStatus");
    return res.status(500).json({
      status: "error",
      message: "Internal server error",
    });
  }

  const { userId } = req.params;

  if (!userId) {
    return res.status(400).json({
      status: "failed",
      message: "Insuffient data",
    });
  }

  const data = req.body as Pengguna;

  const newUser: Pengguna = {
    id_pengguna: user.id_pengguna,
    nama: data.nama ?? user.nama,
    email: data.email ?? user.email,
    fakultas: data.fakultas ?? user.fakultas,
    institusi: data.institusi ?? user.institusi,
    is_admin: data.is_admin ?? user.is_admin,
    is_blocked: data.is_blocked ?? user.is_blocked,
  };

  try {
    await prisma.pengguna.update({
      where: {
        id_pengguna: userId,
      },
      data: newUser,
    });

    res.json({
      status: "success",
      message: "User account edited",
    });
  } catch (err) {
    console.error(`[ERROR] ${err}`);
    res.status(500).json({
      message: "Internal server error",
      status: "error",
    });
  }
}
